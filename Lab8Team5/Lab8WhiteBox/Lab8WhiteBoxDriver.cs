﻿using System;

namespace Lab8WhiteBox
{
	class Lab8WhiteBoxDriver
	{
		static void Main(string[] args)
		{
			Console.WriteLine("oddity(32765, 32767)");
			Console.WriteLine("Expected: Error or 65532");
			Console.WriteLine("Actual:");
			Lab8WhiteBoxTests.OverFlowTest();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(-32768, -32765)");
			Console.WriteLine("Expected: -65532");
			Console.WriteLine("Actual:");
			Lab8WhiteBoxTests.UnderFlowTest();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(2, 1)");
			Console.WriteLine("Expected: Error, Max is smaller than start.");
			Console.WriteLine("Actual:");
			Lab8WhiteBoxTests.ErrorTest();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();
		}
	}
}
