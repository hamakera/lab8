﻿using System;

namespace Lab8Team4
{
	public class Lab8Team4
    {
		static public void oddity(short start, short max)
		{

			if (max > start)
			{
				int total = 0;
				//specs use [] so gotta be inclusive
				for (int i = start; i <= max; i++)
				{
					if (i % 2 != 0)
					{
						Console.WriteLine(i);
						total += i;
					}
				};
				Console.WriteLine("Total: " + total);
			}
			else
			{
				Console.WriteLine("Error, Max is smaller than start.");
			}
		}

	}
}

