﻿using static Lab8Team4.Lab8Team4;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab8WhiteBox
{
	/// <summary>
	/// Range: [-32768, 32767]
	/// </summary>
	class Lab8WhiteBoxTests
	{
		/// <summary>
		/// Overs the flow test. Expect either error or 65,532
		/// [32765,32767]
		/// </summary>
		public static void OverFlowTest()
		{
			oddity(32765, 32767);
		}

		/// <summary>
		/// Unders the flow test. Expected: -65,532
		/// [-32768,-32765]
		/// </summary>
		public static void UnderFlowTest()
		{
			oddity(-32768, -32765);
		}

		/// <summary>
		/// Errors the test. Expected: Error, Max is smaller than start.
		/// [2,1]
		/// </summary>
		public static void ErrorTest()
		{
			oddity(2, 1);
		}
	}
}
