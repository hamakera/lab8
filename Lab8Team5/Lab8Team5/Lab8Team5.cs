﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8Team5
{
    public class Lab8Team5
    {
        public static void righter(sbyte max)
        {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < max; i++)
			{
				int numSpace = max - i;
				for (int j = 0; j < max; j++)
				{
					if (numSpace > 0)
						sb.Append(".");
					else
						sb.Append(String.Format("{0,2}", i));
					numSpace--;
				}
				sb.Append("\n");
			}
			Console.WriteLine(sb.ToString());
        }
    }
}
