﻿using System;
using System.Threading;

namespace Lab8BlackBox
{
	class Lab8BlackBoxDriver
	{
		static void Main(string[] args)
		{
			Console.WriteLine("oddity(1,20)");
			Console.WriteLine("Expected: 100");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.ComplexInRangePositive();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(-20,20)");
			Console.WriteLine("Expected: 0");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.ComplexInRangeNegative();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(-1,3)");
			Console.WriteLine("Expected: 3");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.SimpleInRangeNegative();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(1,2)");
			Console.WriteLine("Expected: 1");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.SimpleInRangePositive();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(50,1)");
			Console.WriteLine("Expected: ERROR??");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.StartBigEndSmallPositive();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(1,20)");
			Console.WriteLine("Expected: ERROR??");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.StartPosEndNeg();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(-2,-1)");
			Console.WriteLine("Expected: -1");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.TwoNeg();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(32765,32767)");
			Console.WriteLine("Expected: error OR 65,532");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.OverFlowTest();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();

			Console.WriteLine("oddity(-1,-50)");
			Console.WriteLine("Expected: ERROR??");
			Console.WriteLine("Actual:");
			Lab8BlackBoxTest.StartBigEndSmallNegative();
			Console.WriteLine("-----------------------------------------------");
			Console.WriteLine();
		}
	}
}
