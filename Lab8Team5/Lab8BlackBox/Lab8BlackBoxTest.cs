﻿using System;
using static Lab8Team4.Lab8Team4;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace Lab8BlackBox
{
	/// <summary>
	/// Range: [-32768, 32767]
	/// </summary>
	class Lab8BlackBoxTest
	{
		/// <summary>
		/// Belows the start. Should print 1, as the only odd number in the range 1-2 is 1 and the sum of 1 is 1
		/// [1,2]
		/// </summary>
		public static void SimpleInRangePositive()
		{
			oddity(1,2);
		}

		/// <summary>
		/// Simples the in range negative. Should print 3 (sum of: -1, 1, 3)
		/// [-1,3]
		/// </summary>
		public static void SimpleInRangeNegative()
		{
			oddity(-1, 3);
			//-1, 1, 3
		}

		/// <summary>
		/// Complexes the in range. Should print 100 (the sum of 1,3,5,7,9,11,13,15,17,19)
		/// [1,20]
		/// </summary>
		public static void ComplexInRangePositive()
		{
			oddity(1, 20);
			//1,3,5,7,9,11,13,15,17,19...
			//total: 100
		}

		/// <summary>
		/// Complexes the in range positive. should print 0....
		/// [-20,20]
		/// </summary>
		/// <returns></returns>
		public static void ComplexInRangeNegative()
		{
			oddity(-20, 20);
		}

		/// <summary>
		/// Start the big end small. Should produce error
		/// [50,1]
		/// </summary>
		public static void StartBigEndSmallPositive()
		{
			oddity(50, 1);
		}


		/// <summary>
		/// Starts the position end neg. Should produce error
		/// [1,-2]
		/// </summary>
		public static void StartPosEndNeg()
		{
			oddity(1, -2);
		}

		/// <summary>
		/// Twoes the neg. Should produce: -1
		/// [-2,-1]
		/// </summary>
		public static void TwoNeg()
		{
			oddity(-2, -1);
		}

		

		/// <summary>
		/// Starts the big end small positive. Expected: error
		/// [-1,-50]
		/// </summary>
		public static void StartBigEndSmallNegative()
		{
			oddity(-1, -50);
		}
	}
}
